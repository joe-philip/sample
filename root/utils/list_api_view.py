from rest_framework.generics import GenericAPIView
from rest_framework.mixins import ListModelMixin
from rest_framework.request import Request
from rest_framework.response import Response

from root.utils.utils import success, success_encrypted


class ListAPIView(ListModelMixin, GenericAPIView):
    def get(self, request: Request, *args, **kwargs) -> Response:
        response: Response = super().list(request, *args, **kwargs)
        response.data = success_encrypted(
            request.user.key, response.data) if request.user.is_authenticated else success(response.data)
        return response
