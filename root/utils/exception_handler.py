from logging import error
from binascii import Error
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.db import connections
from django.db.models.deletion import ProtectedError
from django.http import Http404
from rest_framework import exceptions
from rest_framework.exceptions import APIException
from rest_framework.response import Response

from .utils import fail


def custom_exception_handler(exc, context):

    # Returns the response that should be used for any given exception.
    #
    # By default we handle the REST framework `APIException`, and also
    # Django's built-in `Http404` and `PermissionDenied` exceptions.
    #
    # Any unhandled exceptions may return `None`, which will cause a 500 error
    # to be raised.

    if isinstance(exc, Http404):
        exception = APIException()
        exception.status_code = 404
        exception.detail = 'Object not found'
    elif isinstance(exc, PermissionDenied):
        exception = APIException()
        exception.status_code = 403
        exception.detail = 'Permission denied'
    elif isinstance(exc, APIException):
        exception = exc
    elif isinstance(exc, ProtectedError):
        exception = APIException()
        message = 'Unable to delete object because the referrenced object is being used by {instance} with id {id}'
        instance = tuple(exc.protected_objects)
        exception.detail = message.format(
            instance=instance[0]._meta.verbose_name,
            id=instance[0]
        )
        exception.status_code = 400
    elif isinstance(exc, Error):
        exception = APIException()
        exception.detail = 'Invalid request'
        exception.status_code = 400
    else:
        if settings.DEBUG:
            print(str(exc))
        error(msg=str(exc)+f'\n{type(exc)}')
        exception = APIException()
        exception.status_code = 500
        # exception.detail = 'Server error please contact your system Administrator'
        exception.detail = str(exc)

    if isinstance(exception, exceptions.APIException):
        headers = {}
        if getattr(exc, 'auth_header', None):
            headers['WWW-Authenticate'] = exc.auth_header
        if getattr(exc, 'wait', None):
            headers['Retry-After'] = '%d' % exc.wait

        if isinstance(exception.detail, (list, dict)):
            data = fail(exception.detail)
        else:
            data = fail(str(exception.detail))

        set_rollback()
        return Response(data, status=exception.status_code, headers=headers)

    return None


def set_rollback():
    for db in connections.all():
        if db.settings_dict['ATOMIC_REQUESTS'] and db.in_atomic_block:
            db.set_rollback(True)
