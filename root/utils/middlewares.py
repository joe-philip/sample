from base64 import b64decode
from django.core.handlers.wsgi import WSGIRequest


class Base64DecodeMiddleware:
    def __init__(self, get_response) -> None:
        self.get_response = get_response

    def __call__(self, request: WSGIRequest):
        request.body = b64decode(request.body)
        response = self.get_response(request)
        return response
