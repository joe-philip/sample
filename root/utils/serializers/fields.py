from rest_framework import serializers


class Base64ImageField(serializers.CharField):
    from PIL.JpegImagePlugin import JpegImageFile
    from PIL.PngImagePlugin import PngImageFile

    def to_internal_value(self, value) -> JpegImageFile | PngImageFile:
        from base64 import b64decode
        from io import BytesIO
        from re import match

        from PIL.Image import open

        pattern = r'^data:image\/(jpe?|pn)g;base64,(.+)'
        re_match = match(pattern, value)
        image = open(BytesIO(b64decode(re_match.group(2))))
        return image
