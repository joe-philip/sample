from rest_framework import HTTP_HEADER_ENCODING
from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.request import Request


class QueryparamAuthentication(TokenAuthentication):
    def authenticate(self, request: Request):
        if request.query_params.get('auth'):
            auth = f'Token {request.query_params.get("auth")}'
        else:
            auth = request.META.get('HTTP_AUTHORIZATION', '')
        auth = auth.encode(HTTP_HEADER_ENCODING).split()
        if not auth or auth[0].lower() != self.keyword.lower().encode():
            return None
        if len(auth) == 1:
            msg = 'Invalid token header. No credentials provided.'
            raise AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = 'Invalid token header. Token string should not contain spaces.'
            raise AuthenticationFailed(msg)
        try:
            token = auth[1].decode()
        except UnicodeError:
            msg = 'Invalid token header. Token string should not contain invalid characters.'
            raise AuthenticationFailed(msg)
        print(token)
        return self.authenticate_credentials(token)