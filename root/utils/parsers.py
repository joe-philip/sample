import base64

from Crypto.Cipher import AES
from Crypto.Util.Padding import unpad
from django.core.handlers.wsgi import WSGIRequest
from rest_framework.parsers import BaseParser


def decrypt(enc, key):
    enc = base64.b64decode(enc)
    cipher = AES.new(key.encode('utf-8'), AES.MODE_ECB)
    value = unpad(cipher.decrypt(enc), 16)
    return value


class EncyptedTextParser(BaseParser):
    media_type = 'text/plain'

    def parse(self, stream: WSGIRequest, media_type=None, parser_context=None):
        data = eval(decrypt(stream.body, stream.user.key).decode())
        return data
