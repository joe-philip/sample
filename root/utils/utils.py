def encrypt(raw, key):
    from base64 import b64encode

    from Crypto.Cipher import AES
    from Crypto.Util.Padding import pad

    raw = pad(raw.encode(), 16)
    cipher = AES.new(key.encode('utf-8'), AES.MODE_ECB)
    return b64encode(cipher.encrypt(raw))


def success_encrypted(key: str, data=None) -> dict:
    from json import dumps

    response = {
        'status': True,
        'message': 'success'
    }
    if data:
        response['data'] = encrypt(dumps(data), key)
    return response


def success(data=None) -> dict:
    response = {
        'status': True,
        'message': 'success'
    }
    if data:
        response['data'] = data
    return response


def fail(error) -> dict:
    response = {
        'status': False,
        'message': 'fail',
        'error': error
    }
    return response


def slug_generate(key: str = 'slug') -> str:
    from datetime import datetime

    from django.template.defaultfilters import slugify

    return slugify(f'{key}-{datetime.now().timestamp()}')
