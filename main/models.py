from root.utils.utils import slug_generate
from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.


class User(AbstractUser):
    key = models.CharField(max_length=16, null=True)

    def __str__(self) -> str:
        return f'{self.first_name.capitalize()} {self.last_name.capitalize()}'


class UserProfileModel(models.Model):
    gender_choices = (
        (1, 'Male'),
        (2, 'Female'),
        (3, 'Others')
    )
    slug = models.SlugField(unique=True, db_index=True)
    profile_image = models.ImageField(
        upload_to='profile_images', null=True, blank=True
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    gender = models.IntegerField(choices=gender_choices)

    def save(self, *args, **kwargs):
        if self.slug in (None, ''):
            self.slug = slug_generate('user-profile')
        return super().save(*args, **kwargs)

    def __str__(self) -> str:
        return f'{self.user.first_name.capitalize()} {self.user.last_name.capitalize()}'


class PersonalGallery(models.Model):
    user = models.ForeignKey(UserProfileModel, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='gallery')
