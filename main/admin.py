from django.contrib import admin
from django.utils.html import format_html
from .models import User, UserProfileModel

# Register your models here.


class UserAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'username', 'email',
        'profile_activated', 'view_profile'
    )
    search_fields = ('first_name', 'last_name', 'username')
    readonly_fields = ('profile_activated',)
    ordering = ('date_joined',)
    list_per_page = 10

    def name(self, obj: User):
        return f'{obj.first_name.capitalize()} {obj.last_name.capitalize()}'

    def profile_activated(self, obj: User):
        return UserProfileModel.objects.filter(user=obj).exists()
    profile_activated.boolean = True

    def view_profile(self, obj: User):
        if (profile := UserProfileModel.objects.filter(user=obj)).exists():
            return format_html(f'<a href="/admin/main/userprofilemodel/{profile.first().id}/change/">View Profile</a>')
        return 'NA'
    view_profile.allow_tags = True

    class Meta:
        model = User


admin.site.register(User, UserAdmin)


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('name', 'username', 'view_user')
    search_fields = ('user__first_name', 'user__last_name', 'user__username')

    def name(self, obj: UserProfileModel):
        return f'{obj.user.first_name.capitalize()} {obj.user.last_name.capitalize()}'

    def username(self, obj: UserProfileModel):
        return obj.user.username

    def view_user(self, obj: UserProfileModel):
        return format_html(f'<a href="/admin/main/user/{obj.user.id}/change/">View user data</a>')

    class Meta:
        model = UserProfileModel


admin.site.register(UserProfileModel, UserProfileAdmin)
