from django.urls import path

from . import views

urlpatterns = [
    path('signup', views.SignupAPI.as_view()),
    path('login', views.LoginAPI.as_view()),
    path('dashboard', views.DashboardAPI.as_view()),
    path('user_profile', views.ProfileUpdateAPI.as_view()),
    path(
        'media/profile_images/<str:file_name>', views.ProfileImageAPI.as_view()
    ),
    path('profile_image_update', views.ProfileImageAPI.as_view()),
    path('users', views.UserListAPI.as_view()),
    path('gallery', views.UserGalerryAPI.as_view())
]
