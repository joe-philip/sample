from datetime import datetime
from os.path import join

from django.conf import settings
from django.http.response import FileResponse
from django.shortcuts import get_object_or_404
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from root.utils.list_api_view import ListAPIView
from root.utils.utils import success, success_encrypted

from .models import PersonalGallery, User, UserProfileModel
from .serializers import ProfileResponseSerializer, UserResponseSerializer

# Create your views here.


class SignupAPI(APIView):
    parser_classes = (JSONParser,)

    def post(self, r: Request) -> Response:
        from .serializers import SignupSerializer

        serializer = SignupSerializer(data=r.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.signup()
        data = UserResponseSerializer(user).data
        return Response(success(data), status=201)


class LoginAPI(APIView):
    parser_classes = (JSONParser,)

    def post(self, r: Request) -> Response:
        from random import choices
        from string import ascii_uppercase, digits

        from django.contrib.auth import authenticate
        from rest_framework.authtoken.models import Token
        from rest_framework.exceptions import AuthenticationFailed

        from .serializers import LoginSerializer

        serializer = LoginSerializer(data=r.data)
        serializer.is_valid(raise_exception=True)
        user: User | None = authenticate(
            r, username=serializer.validated_data.get('username'),
            password=serializer.validated_data.get('password')
        )
        if user:
            user.key = ''.join(choices(ascii_uppercase + digits, k=16))
            user.last_login = datetime.now()
            user.save()
            (token, _) = Token.objects.get_or_create(user=user)
            data = success(UserResponseSerializer(user).data)
            data['tokens'] = {'access': token.key}
            return Response(data)
        raise AuthenticationFailed()


class DashboardAPI(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, r: Request) -> Response:
        profile = get_object_or_404(UserProfileModel, user=r.user)
        data = ProfileResponseSerializer(profile, context={'request': r}).data
        return Response(success_encrypted(r.user.key, data))


class ProfileUpdateAPI(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, r: Request) -> Response:
        data = UserResponseSerializer(r.user).data
        return Response(success_encrypted(r.user.key, data))

    def post(self, r: Request) -> Response:

        from .serializers import ProfileUpdateSerializer

        serializer = ProfileUpdateSerializer(data=r.data)
        serializer.is_valid(raise_exception=True)
        (profile, _) = UserProfileModel.objects.get_or_create(
            user=(user := r.user),
            defaults={'gender': 1}
        )
        if profile_image := serializer.validated_data.get('profile_image'):
            path = f'profile_images/{profile.id}.{profile_image.format.lower()}'
            profile_image.save(join(settings.BASE_DIR, f'media/{path}'))
            profile.profile_image = path
        user.first_name = serializer.validated_data.get('first_name')
        user.last_name = serializer.validated_data.get('last_name')
        user.email = serializer.validated_data.get('email')
        profile.save(), user.save()
        data = ProfileResponseSerializer(profile, context={'request': r}).data
        return Response(success_encrypted(user.key, data))


class LogoutAPI(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, r: Request) -> Response:
        r.user.authtoken.delete()
        return Response(success())


class ProfileImageAPI(APIView):
    from root.utils.authentication import QueryparamAuthentication

    from .permissions import ProfileImageAccessPermissions

    permission_classes = (IsAuthenticated, ProfileImageAccessPermissions)
    authentication_classes = (QueryparamAuthentication,)

    def get(self, r: Request, file_name: str) -> FileResponse:
        obj = get_object_or_404(
            UserProfileModel, profile_image=f'profile_images/{file_name}', user=r.user
        )
        self.check_object_permissions(r, obj)
        return FileResponse(obj.profile_image)

    def post(self, request: Request) -> Response:
        from .serializers import ProfileImageUpdateSerializer

        serializer = ProfileImageUpdateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        (profile, _) = UserProfileModel.objects.get_or_create(
            user=request.user,
            gender=1
        )
        profile_image = serializer.validated_data.get('profile_image')
        path = f'profile_images/{profile.id}.{profile_image.format.lower()}'
        profile_image.save(join(settings.BASE_DIR, f'media/{path}'))
        profile.profile_image = path
        profile.save()
        return Response(success_encrypted(key=request.user.key, data=path))


class UserListAPI(ListAPIView):
    serializer_class = UserResponseSerializer

    def get_queryset(self):
        from django.core.cache import cache

        return cache.get_or_set('users', User.objects.all(), timeout=60*60)


class UserGalerryAPI(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request: Request) -> Response:
        from .serializers import GallerySerializer, UserMediaListSerializer

        serializer = GallerySerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        images = serializer.validated_data.get('images')
        profile = get_object_or_404(UserProfileModel, user=request.user)
        data = tuple(map(lambda x: self.save(profile, x), images))
        PersonalGallery.objects.bulk_create(data)
        response_data = UserMediaListSerializer(
            instance=request.user, context={'request': request}).data
        return Response(success_encrypted(request.user.key, response_data))

    def save(self, user: UserProfileModel, image) -> PersonalGallery:
        import os

        from django.template.defaultfilters import slugify

        path = f'gallery/{user.id}/{slugify(datetime.now().timestamp())}.{image.format.lower()}'
        if not os.path.exists(join(settings.BASE_DIR, f'media/gallery/{user.id}')):
            os.makedirs(join(settings.BASE_DIR, f'media/gallery/{user.id}'))
        image.save(join(settings.BASE_DIR, f'media/{path}'))
        return PersonalGallery(user=user, image=path)
