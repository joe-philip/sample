from rest_framework.permissions import BasePermission
from rest_framework.request import Request


class ProfileImageAccessPermissions(BasePermission):
    message = 'You do not have access to this image'
    from .models import UserProfileModel

    def has_object_permission(self, request:Request, view, obj:UserProfileModel):
        print(obj.user == request.user)
        return obj.user == request.user
