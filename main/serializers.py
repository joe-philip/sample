from rest_framework import serializers

from root.utils.serializers.fields import Base64ImageField

from .models import PersonalGallery, User, UserProfileModel

# Create your serializers here.


class SignupSerializer(serializers.Serializer):
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    email = serializers.EmailField()
    username = serializers.CharField()
    password = serializers.CharField()

    def validate_username(self, value: str) -> str:
        if User.objects.filter(username=value).exists():
            raise serializers.ValidationError('Username taken')
        if value.find(' ') != -1:
            raise serializers.ValidationError(
                'Spaces are not allowed for usernames')
        return str(value).lower()

    def validate_email(self, value: str) -> str: return str(value).lower()

    def validate_password(self, value: str) -> str:
        from django.contrib.auth.password_validation import (
            CommonPasswordValidator, MinimumLengthValidator,
            NumericPasswordValidator)

        from root.utils.password_validation import (
            PasswordRegexValidation, PasswordSpecialCharacterValidation)

        validators: tuple = (
            CommonPasswordValidator(), MinimumLengthValidator(),
            NumericPasswordValidator(), PasswordRegexValidation(),
            PasswordSpecialCharacterValidation()
        )
        tuple(map(lambda x: x.validate(value), validators))
        return value

    def signup(self) -> User:
        return User.objects.create_user(**self.validated_data)


class UserResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        exclude = ('password',)


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()


class ProfileUpdateSerializer(serializers.Serializer):
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    email = serializers.EmailField()
    profile_image = Base64ImageField(required=False)


class ProfileResponseSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        data = super().to_representation(instance)
        if data.get('user'):
            if data['user'].get('password'):
                del data['user']['password']
        return data

    class Meta:
        model = UserProfileModel
        fields = '__all__'
        depth = 1


class ProfileImageUpdateSerializer(serializers.Serializer):
    profile_image = Base64ImageField()


class GallerySerializer(serializers.Serializer):
    images = serializers.ListField(child=Base64ImageField())


class GalleryResponseSerializer(serializers.Serializer):
    class Meta:
        model = PersonalGallery
        exclude = ('user',)


class UserMediaListSerializer(serializers.ModelSerializer):
    images = serializers.SerializerMethodField()

    class Meta:
        model = User
        exclude = ('password',)

    def get_images(self, instance):
        queryset = PersonalGallery.objects.filter(
            user__user=self.context.get('request').user
        )
        return GalleryResponseSerializer(queryset, many=True, context=self.context).data
